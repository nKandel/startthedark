from django.conf.urls.defaults import *
#from welcome.views import hello
from django.contrib import admin
admin.autodiscover() #ADD TO REMOVE You don't have permission to edit anything ERROR

urlpatterns = patterns('',
	(r'^events/',include('events.urls')),
    (r'^admin/', include(admin.site.urls)),#admin.site.root is replace by admin.site.urls for later version

)
